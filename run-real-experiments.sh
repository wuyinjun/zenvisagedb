N_EXPER=200
schema="/home/tsiddiq2/data/airline_schema.csv"
dataset="/home/tsiddiq2/data/airline.csv"
limit=10000000
sample_size=8000
intersection_sample_size=100
multiple=1;
shopt -s globstar
javac -cp "lib/*:src/**" -d bin **/*.java
java -Xms5g -Xmx40g -d64   -cp  ./bin/:./lib/*  -javaagent:./lib/SizeOf.jar org/vde/database/BenchmarkReal $dataset $schema $limit $N_EXPER $sample_size $intersection_sample_size $multiple