package org.vde.database;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.roaringbitmap.RoaringBitmap;
import org.roaringbitmap.SampledResult;

import net.sourceforge.sizeof.SizeOf;

public class BenchmarkSynthetic {

	static Database db;
	static SamplingBasedExecutor samplingBasedExecutor;
	static Executor executor;
	static Random randomGenerator = new Random();
	static boolean sizeof;
	static int limit = 1000000;
	static int NTRIALS = 30;
	static String name = "flightDataset";
	static String schema = "data/syntheticdata_schema.csv";
	static String dataset = "data/airline.csv";
	static int sample_size = 8000;
	static int intersection_sample_size = 100;
	static int multiple = 0;
	static double[] densities = new double[] { 0.001, 0.005, 0.01, 0.05, 0.1,
			0.25, 0.5, 0.75, 0.9 };

	public static void printSystemDetails() {
		System.out.println("########");
		System.out.println("# " + System.getProperty("java.vendor") + " "
				+ System.getProperty("java.version") + " "
				+ System.getProperty("java.vm.name"));
		System.out.println("# " + System.getProperty("os.name") + " "
				+ System.getProperty("os.arch") + " "
				+ System.getProperty("os.version"));
		System.out.println("# processors: "
				+ Runtime.getRuntime().availableProcessors());
		System.out.println("# max mem.: " + Runtime.getRuntime().maxMemory());
		System.out.println("########");
	}

	@SuppressWarnings("javadoc")
	public static void main(final String[] args) throws IOException,
	InterruptedException {
		Locale.setDefault(Locale.US);
		printSystemDetails();
		if (args.length == 6) {
			schema = args[0];
			limit = Integer.parseInt(args[1]);
			NTRIALS = Integer.parseInt(args[2]);
			sample_size = Integer.parseInt(args[3]);
			intersection_sample_size = Integer.parseInt(args[4]);
			multiple = Integer.parseInt(args[5]);
		}

		sizeof = true;
		try {
			SizeOf.setMinSizeToLog(0);
			SizeOf.skipStaticField(true);
			// SizeOf.skipFinalField(true);
			SizeOf.deepSizeOf(args);
		} catch (IllegalStateException e) {
			sizeof = false;
			System.out
			.println("# disabling sizeOf, run  -javaagent:lib/SizeOf.jar or equiv. to enable");

		}

		// loadSyntheticData("uniform", limit, 0.3);

   //     testWithReplacement();
        testWithOutReplacement();
		

	}

	public static void testWithOutReplacement() throws IOException,
	InterruptedException {
		loadSyntheticData("uniform", limit, 0.3);
		boolean flag = true;
		for (int l = 0; l < densities.length; l++) {
			double density = densities[l];
			if (flag) {
				int setBits = getAvgSetBits("Day", NTRIALS);
				int sample = (int) (0.01 * setBits);
				System.out
				.println("Warming up Dry run: Simple Aggregation: Sampling : with a sample size of "
						+ sample + " Percentage :" + 0.01);
				testSimpleAggregation("Day", "ArrDelay", sample, NTRIALS);
				flag = false;
			}
			System.out.println("Loading data with density " + density
					+ " and max integer:" + limit);
			loadSyntheticData("uniform", limit, density);

			int count = 0;
			if (multiple == 1) {
				double[] data = new double[] { .01, .10, .25, .50, .75, .90, 1 };
				int setBits = getAvgSetBits("Day", NTRIALS);
				System.out
				.println("Average number of setbits per Roaring bitmap for Day attribute:"
						+ setBits);
				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * setBits);
					System.out.println("Simple Aggregation: Sampling "
							+ (i + 1) + ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testSimpleAggregationWithoutReplacement("Day", "ArrDelay",
							sample, NTRIALS);
					testSimpleAggregationWithoutReplacementMultipleRounds(
							"Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length - 3; i++) {
					int sample = (int) (data[i] * db.rowCount);
					System.out.println("Simple Aggregation: Sampling "
							+ (data.length + i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testSimpleAggregationWithoutReplacement("Day", "ArrDelay",
							sample, NTRIALS);
					testSimpleAggregationWithoutReplacementMultipleRounds(
							"Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * setBits);
					System.out.println("OR Aggregation: Sampling " + (i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testORAggregationWithoutReplacement("Day", "Day",
							"ArrDelay", sample, NTRIALS);
					testORAggregationWithoutReplacementMultipleRounds("Day",
							"Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length - 3; i++) {
					int sample = (int) (data[i] * db.rowCount);
					System.out.println("OR Aggregation: Sampling "
							+ (data.length + i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testORAggregationWithoutReplacement("Day", "Day",
							"ArrDelay", sample, NTRIALS);
					testORAggregationWithoutReplacementMultipleRounds("Day",
							"Day", "ArrDelay", sample, NTRIALS);
				}

				int intersectionBits = (int) getAvgIntersectionSize("Day",
						"Day", NTRIALS);
				System.out
				.println("Average number of intersection for Day and Day attributes:"
						+ intersectionBits);
				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * intersectionBits);
					System.out.println("AND Aggregation: Sampling " + (i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testANDAggregationWithoutReplacement("Day",
							"Day", "ArrDelay", sample, NTRIALS);
					testANDAggregationWithoutReplacementMultipleRounds("Day",
							"Day", "ArrDelay", sample, NTRIALS);
				}
				int sample = (int) (db.rowCount * 0.25);
				System.out.println("AND Aggregation: Sampling "
						+ (data.length + 1) + ": with a sample size of "
						+ sample + " Percentage :" + sample);
				testANDAggregationWithoutReplacement("Day", "Day",
						"ArrDelay", sample, NTRIALS);
				testANDAggregationWithoutReplacementMultipleRounds("Day",
						"Day", "ArrDelay", sample, NTRIALS);

			} else {
				testSimpleAggregationWithoutReplacement("Day", "ArrDelay",
						sample_size, NTRIALS);
				testORAggregationWithoutReplacement("Day", "Day",
						"ArrDelay", sample_size, NTRIALS);
				testANDAggregationWithoutReplacement("Day", "Day",
						"ArrDelay", intersection_sample_size, NTRIALS);
			}
		}
	}

	public static void testWithReplacement() throws IOException, InterruptedException {
		boolean flag = true;
		loadSyntheticData("uniform", limit, 0.3);
		for (int l = 0; l < densities.length; l++) {
			double density = densities[l];
			if (flag) {
				int setBits = getAvgSetBits("Day", NTRIALS);
				int sample = (int) (0.01 * setBits);
				System.out
				.println("Warming up Dry run: Simple Aggregation: Sampling : with a sample size of "
						+ sample + " Percentage :" + 0.01);
				testSimpleAggregation("Day", "ArrDelay", sample, NTRIALS);
				flag = false;
			}
			System.out.println("Loading data with density " + density
					+ " and max integer:" + limit);
			loadSyntheticData("uniform", limit, density);

			int count = 0;
			if (multiple == 1) {
				double[] data = new double[] { .01, .10, .25, .50, .75, .90 };
				int setBits = getAvgSetBits("Day", NTRIALS);
				System.out
				.println("Average number of setbits per Roaring bitmap for Day attribute:"
						+ setBits);
				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * setBits);
					System.out.println("Simple Aggregation: Sampling "
							+ (i + 1) + ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testSimpleAggregation("Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length - 3; i++) {
					int sample = (int) (data[i] * db.rowCount);
					System.out.println("Simple Aggregation: Sampling "
							+ (data.length + i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testSimpleAggregation("Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * setBits);
					System.out.println("OR Aggregation: Sampling " + (i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testORAggregation("Day", "Day", "ArrDelay", sample, NTRIALS);
				}

				for (int i = 0; i < data.length - 3; i++) {
					int sample = (int) (data[i] * db.rowCount);
					System.out.println("OR Aggregation: Sampling "
							+ (data.length + i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testORAggregation("Day", "Day", "ArrDelay", sample, NTRIALS);
				}

				int intersectionBits = (int) getAvgIntersectionSize("Day",
						"Day", NTRIALS);
				System.out
				.println("Average number of intersection for Day and Day attributes:"
						+ intersectionBits);
				for (int i = 0; i < data.length; i++) {
					int sample = (int) (data[i] * intersectionBits);
					System.out.println("AND Aggregation: Sampling " + (i + 1)
							+ ": with a sample size of " + sample
							+ " Percentage :" + data[i]);
					testANDAggregation("Day", "Day", "ArrDelay", sample,
							NTRIALS);
				}
				int sample = (int) (db.rowCount * 0.25);
				System.out.println("AND Aggregation: Sampling "
						+ (data.length + 1) + ": with a sample size of "
						+ sample + " Percentage :" + sample);
				testANDAggregation("Day", "Day", "ArrDelay", sample, NTRIALS);

			} else {
				testSimpleAggregation("Day", "ArrDelay", sample_size, NTRIALS);
				testORAggregation("Day", "Day", "ArrDelay", sample_size,
						NTRIALS);
				testANDAggregation("Day", "Day", "ArrDelay",
						intersection_sample_size, NTRIALS);
			}

		}
	}

	private static void loadSyntheticData(String type, int length, double d)
			throws IOException, InterruptedException {

		long start = System.currentTimeMillis();
		db = new Database("test", schema);
		DataGenerator dGen = new DataGenerator(length);
		if (type.equals("uniform")) {
			System.out.println("Loading synthetic data: " + type
					+ " distribution");
			for (int i = 0; i < 50; i++) {
				int[] uniformdata = dGen.getUniform(d);
				for (int row = 0; row < uniformdata.length; row++)
					db.addValue("Day", uniformdata[row], String.valueOf(i));
			}
		} else {
			System.out.println("Loading synthetic data: " + type
					+ " distribution");
			for (int i = 0; i < 50; i++) {
				int[] zipdata = dGen.getZipfian(d);
				for (int row = 0; row < zipdata.length; row++)
					db.addValue("Day", zipdata[row], String.valueOf(i));

			}
		}

		// Measure attribute
		for (int i = 0; i < length + 1; i++) {
			db.addValue("ArrDelay", i,
					String.valueOf(randomGenerator.nextInt(10)));
		}

		long duration = System.currentTimeMillis() - start;
		samplingBasedExecutor = new SamplingBasedExecutor(db);
		executor = new Executor(db);
		db.rowCount = length + 1;
		printDbDetails(db, duration);
	}

	private static void printDbDetails(Database db, long time) {

		long row_count = db.rowCount;
		long numberofColumns = db.getColumns().size();
		if (!sizeof)
			return;

		long dbSize = SizeOf.deepSizeOf(db);
		System.out.println("########");
		System.out.println("Number of rows in the database: " + row_count);
		System.out.println("Number of columns in the database: "
				+ numberofColumns);
		System.out.println("Size of the database: " + dbSize);
		System.out.println("Time taken to load the database: " + time);
		System.out.println("########");
	}

	public static int getAvgSetBits(String groupbyAttribute, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		int count = 0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}

			RoaringBitmap bitmap = samplingBasedExecutor.getSimpleGroup(
					groupbyAttribute, value);
			count = count + bitmap.getCardinality();
		}

		return count / numberofruns;
	}

	public static void testSimpleAggregation(String groupbyAttribute,
			String measureAtttribute, int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		int sampleduniqueContainers = 0;
		int sampledtotalContainers = 0;
		int totalConatiners = 0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}

			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getSampledSimpleGroup(groupbyAttribute, value,
							numberofSamples);
			// System.out.println(sampledResult.sampledContainers.size());

			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			RoaringBitmap roaringBitmap = samplingBasedExecutor.getSimpleGroup(
					groupbyAttribute, value);
			samplingBasedExecutor.getSimpleAggregate(roaringBitmap,
					measureAtttribute, "avg");
			long fourth = System.nanoTime();
			sampleduniqueContainers = sampleduniqueContainers
					+ sampledResult.sampledContainers.size();
			sampledtotalContainers = sampledtotalContainers
					+ sampledResult.totalContainers;
			totalConatiners = totalConatiners
					+ roaringBitmap.highLowContainer.size();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		sampleduniqueContainers = sampleduniqueContainers / numberofruns;
		totalConatiners = totalConatiners / numberofruns;
		sampledtotalContainers = sampledtotalContainers / numberofruns;
		System.out.println("##Testing Simple GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("Average number of sampled unique Containers:"
				+ sampleduniqueContainers);
		System.out.println("Average number of sampled total Containers:"
				+ sampledtotalContainers);
		System.out.println("Average number of total containers:"
				+ totalConatiners);
		System.out.println("########");

	}

	public static void testSimpleAggregationWithoutReplacement(
			String groupbyAttribute, String measureAtttribute,
			int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		int sampleduniqueContainers = 0;
		int sampledtotalContainers = 0;
		int totalConatiners = 0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}

			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getSampledSimpleGroupWithoutReplacement(groupbyAttribute,
							value, numberofSamples);
			// System.out.println(sampledResult.sampledContainers.size());

			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			RoaringBitmap roaringBitmap = samplingBasedExecutor.getSimpleGroup(
					groupbyAttribute, value);
			samplingBasedExecutor.getSimpleAggregate(roaringBitmap,
					measureAtttribute, "avg");
			long fourth = System.nanoTime();
			sampleduniqueContainers = sampleduniqueContainers
					+ sampledResult.sampledContainers.size();
			sampledtotalContainers = sampledtotalContainers
					+ sampledResult.totalContainers;
			totalConatiners = totalConatiners
					+ roaringBitmap.highLowContainer.size();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		sampleduniqueContainers = sampleduniqueContainers / numberofruns;
		totalConatiners = totalConatiners / numberofruns;
		sampledtotalContainers = sampledtotalContainers / numberofruns;
		System.out.println("##Testing Simple GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("Average number of sampled unique Containers:"
				+ sampleduniqueContainers);
		System.out.println("Average number of sampled total Containers:"
				+ sampledtotalContainers);
		System.out.println("Average number of total containers:"
				+ totalConatiners);
		System.out.println("########");

	}

	public static double getAvgIntersectionSize(String groupbyAttribute1,
			String groupbyAttribute2, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);
		int count = 0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}

			RoaringBitmap bitmap = samplingBasedExecutor.getANDGroup(
					groupbyAttribute1, value, groupbyAttribute2, value1);
			count = count + bitmap.getCardinality();
		}

		return count / numberofruns;

	}

	public static void testSimpleAggregationWithoutReplacementMultipleRounds(
			String groupbyAttribute, String measureAtttribute,
			int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		int sampleduniqueContainers = 0;
		int sampledtotalContainers = 0;
		int totalConatiners = 0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}

			long start = System.nanoTime();
			SampledResult sampledResult;
			sampledResult = samplingBasedExecutor
					.getSampledSimpleGroupWithoutReplacement(groupbyAttribute,
							value, numberofSamples);
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			sampleduniqueContainers = sampleduniqueContainers
					+ sampledResult.sampledContainers.size();
			sampledtotalContainers = sampledtotalContainers
					+ sampledResult.totalContainers;
			for (int p = 0; p < 4; p++) {
				sampledResult = samplingBasedExecutor
						.getSampledSimpleGroupWithoutReplacement(
								groupbyAttribute, value, numberofSamples,
								sampledResult.getLastIndex());
				// System.out.println(sampledResult.sampledContainers.size());
				measureValues = db.getUnIndexedColumnValues(measureAtttribute);
				samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
						measureValues, "avg");
				sampleduniqueContainers = sampleduniqueContainers
						+ sampledResult.sampledContainers.size();
				sampledtotalContainers = sampledtotalContainers
						+ sampledResult.totalContainers;

			}
			long third = System.nanoTime();
			for (int p = 0; p < 5; p++) {
				RoaringBitmap roaringBitmap = samplingBasedExecutor
						.getSimpleGroup(groupbyAttribute, value);
				samplingBasedExecutor.getSimpleAggregate(roaringBitmap,
						measureAtttribute, "avg");
				totalConatiners = totalConatiners
						+ roaringBitmap.highLowContainer.size();
			}
			long fourth = System.nanoTime();
			sampledAggregationTime = sampledAggregationTime + third - start;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		sampleduniqueContainers = sampleduniqueContainers / numberofruns;
		totalConatiners = totalConatiners / numberofruns;
		sampledtotalContainers = sampledtotalContainers / numberofruns;
		System.out.println("##Testing Simple GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: "
				+ (5 * numberofSamples));
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("Average number of sampled unique Containers:"
				+ sampleduniqueContainers);
		System.out.println("Average number of sampled total Containers:"
				+ sampledtotalContainers);
		System.out.println("Average number of total containers:"
				+ totalConatiners);
		System.out.println("########");

	}

	public static void testANDAggregation(String groupbyAttribute1,
			String groupbyAttribute2, String measureAtttribute,
			int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getANDSampledGroup(groupbyAttribute1, value,
							groupbyAttribute2, value1, numberofSamples);
			// System.out.println(sampledResult.sampledContainers.size());
			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			samplingBasedExecutor.getANDAggregate(groupbyAttribute1, value,
					groupbyAttribute2, value1, measureAtttribute, "avg",
					numberofSamples);
			long fourth = System.nanoTime();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

	public static void testANDAggregationWithoutReplacement(
			String groupbyAttribute1, String groupbyAttribute2,
			String measureAtttribute, int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getANDSampledGroupWithoutReplacement(groupbyAttribute1,
							value, groupbyAttribute2, value1, numberofSamples);
			// System.out.println(sampledResult.sampledContainers.size());
			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			samplingBasedExecutor.getANDAggregate(groupbyAttribute1, value,
					groupbyAttribute2, value1, measureAtttribute, "avg",
					numberofSamples);
			long fourth = System.nanoTime();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

	public static void testANDAggregationWithoutReplacementMultipleRounds(
			String groupbyAttribute1, String groupbyAttribute2,
			String measureAtttribute, int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getANDSampledGroupWithoutReplacement(groupbyAttribute1,
							value, groupbyAttribute2, value1, numberofSamples);
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");

			for (int p = 0; p < 4; p++) {
				sampledResult = samplingBasedExecutor
						.getANDSampledGroupWithoutReplacement(
								groupbyAttribute1, value, groupbyAttribute2,
								value1, numberofSamples,
								sampledResult.getLastIndex());
				// System.out.println(sampledResult.sampledContainers.size());
				measureValues = db.getUnIndexedColumnValues(measureAtttribute);
				samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
						measureValues, "avg");

			}
			long third = System.nanoTime();
			for (int p = 0; p < 5; p++) {
				samplingBasedExecutor.getANDAggregate(groupbyAttribute1, value,
						groupbyAttribute2, value1, measureAtttribute, "avg",
						numberofSamples);
			}
			long fourth = System.nanoTime();
			sampledAggregationTime = sampledAggregationTime + third - start;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

	public static void testORAggregation(String groupbyAttribute1,
			String groupbyAttribute2, String measureAtttribute,
			int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {

			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getORSampledGroup(groupbyAttribute1, value,
							groupbyAttribute2, value1, numberofSamples);
			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			samplingBasedExecutor.getORAggregate(groupbyAttribute1, value,
					groupbyAttribute2, value1, measureAtttribute, "avg",
					numberofSamples);
			long fourth = System.nanoTime();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

	public static void testORAggregationWithoutReplacement(
			String groupbyAttribute1, String groupbyAttribute2,
			String measureAtttribute, int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getORSampledGroupWithoutReplacement(groupbyAttribute1,
							value, groupbyAttribute2, value1, numberofSamples);
			// System.out.println(sampledResult.sampledContainers.size());
			long second = System.nanoTime();
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");
			long third = System.nanoTime();
			samplingBasedExecutor.getORAggregate(groupbyAttribute1, value,
					groupbyAttribute2, value1, measureAtttribute, "avg",
					numberofSamples);
			long fourth = System.nanoTime();
			groupbySamplingTime = groupbySamplingTime + second - start;
			sampledAggregationTime = sampledAggregationTime + third - second;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		groupbySamplingTime = groupbySamplingTime / (numberofruns * 1e+6);
		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + numberofSamples);
		System.out.println("Average time for sampling group by  "
				+ groupbySamplingTime);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

	public static void testORAggregationWithoutReplacementMultipleRounds(
			String groupbyAttribute1, String groupbyAttribute2,
			String measureAtttribute, int numberofSamples, int numberofruns) {
		Map<String, RoaringBitmap> indexedbitmaps = db
				.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db
				.getIndexedColumnValues(groupbyAttribute2);

		double groupbySamplingTime = 0.0;
		double sampledAggregationTime = 0.0;
		double unsampledAggregationTime = 0.0;
		for (int i = 0; i < numberofruns; i++) {
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c = 0;
			String value = "";
			for (String key : indexedbitmaps.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value = key;
				break;
			}
			c = 0;
			String value1 = "";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for (String key : indexedbitmaps1.keySet()) {
				if (c < randomInt) {
					c++;
					continue;
				}
				value1 = key;
				break;
			}
			long start = System.nanoTime();
			SampledResult sampledResult = samplingBasedExecutor
					.getORSampledGroupWithoutReplacement(groupbyAttribute1,
							value, groupbyAttribute2, value1, numberofSamples);
			String[] measureValues = db
					.getUnIndexedColumnValues(measureAtttribute);
			samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
					measureValues, "avg");

			for (int p = 0; p < 4; p++) {
				sampledResult = samplingBasedExecutor
						.getORSampledGroupWithoutReplacement(groupbyAttribute1,
								value, groupbyAttribute2, value1,
								numberofSamples,
								sampledResult.getLastIndex());
				// System.out.println(sampledResult.sampledContainers.size());
				measureValues = db.getUnIndexedColumnValues(measureAtttribute);
				samplingBasedExecutor.doAggregateOnSampledBits(sampledResult,
						measureValues, "avg");

			}
			long third = System.nanoTime();
			for (int p = 0; p < 5; p++) {
				samplingBasedExecutor.getORAggregate(groupbyAttribute1, value,
						groupbyAttribute2, value1, measureAtttribute, "avg",
						numberofSamples);
			}
			long fourth = System.nanoTime();
			sampledAggregationTime = sampledAggregationTime + third - start;
			unsampledAggregationTime = unsampledAggregationTime + fourth
					- third;
		}

		sampledAggregationTime = sampledAggregationTime / (numberofruns * 1e+6);
		unsampledAggregationTime = unsampledAggregationTime
				/ (numberofruns * 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "
				+ numberofruns + "  ## Number of samples: " + 5
				* numberofSamples);
		System.out.println("Average time for aggregating sampled group bys: "
				+ sampledAggregationTime);
		System.out.println("Average time for groupby and aggregation samples  "
				+ (groupbySamplingTime + sampledAggregationTime));
		System.out
		.println("Average time for  groupby and aggregation without sampling: "
				+ unsampledAggregationTime);
		System.out.println("########");
	}

}