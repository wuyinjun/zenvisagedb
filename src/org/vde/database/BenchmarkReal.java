package org.vde.database;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.roaringbitmap.RoaringBitmap;
import org.roaringbitmap.SampledContainer;
import org.roaringbitmap.SampledResult;





import net.sourceforge.sizeof.SizeOf;

public class BenchmarkReal {
	
	    static Database db ;
	    static SamplingBasedExecutor samplingBasedExecutor;
	    static Executor executor;
		static Random randomGenerator = new Random();
	    static boolean sizeof ;
	    static int limit=10000000;
	    static int NTRIALS=50;
	    static String name="flightDataset";
		static String schema="data/airline_schema.csv";
		static String dataset="data/airline.csv";
		static int sample_size=8000;
		static int intersection_sample_size=100;
		static int multiple=0;
     
        
        public static void printSystemDetails(){
        	System.out.println("########");
            System.out.println("# " + System.getProperty("java.vendor")
                    + " " + System.getProperty("java.version") + " "
                    + System.getProperty("java.vm.name"));
            System.out.println("# " + System.getProperty("os.name") + " "
                    + System.getProperty("os.arch") + " "
                    + System.getProperty("os.version"));
            System.out.println("# processors: "
                    + Runtime.getRuntime().availableProcessors());
            System.out.println("# max mem.: "
                    + Runtime.getRuntime().maxMemory());
            System.out.println("########");
        }

        @SuppressWarnings("javadoc")
        public static void main(final String[] args) throws IOException, InterruptedException {
                Locale.setDefault(Locale.US);
                printSystemDetails();  
                if(args.length==7){
                dataset = args[0];
                schema = args[1];
                limit = Integer.parseInt(args[2]);
                NTRIALS = Integer.parseInt(args[3]);
                sample_size = Integer.parseInt(args[4]);
                intersection_sample_size = Integer.parseInt(args[5]);
                multiple = Integer.parseInt(args[6]);             
                }
           
                sizeof = true;
                try {
                        SizeOf.setMinSizeToLog(0);
                        SizeOf.skipStaticField(true);
                        // SizeOf.skipFinalField(true);
                        SizeOf.deepSizeOf(args);
                } catch (IllegalStateException e) {
                        sizeof = false;
                        System.out
                                .println("# disabling sizeOf, run  -javaagent:lib/SizeOf.jar or equiv. to enable");

                }
            	
               loadData(dataset, schema, limit); 
       //    	   testWithReplacement();
               testWithOutReplacement();
                 
                   
        }
        

        public static void testWithOutReplacement(){
         	 int setBits= getAvgSetBits("Day",NTRIALS);
          	 int samplet=(int) (0.01*setBits);
        	 System.out.println("Warming up Dry run: Simple Aggregation: Sampling : with a sample size of "+ samplet + " Percentage :"+0.01);            		  
        	 testSimpleAggregationWithoutReplacement("Day","ArrDelay",samplet, NTRIALS);             		
          	         	
              int count=0;
         	  if(multiple==1){
            	   double [] data = new double [] {.01,.10,.25,.50,.75,.90,1};
            	   System.out.println("Average number of setbits per Roaring bitmap for Day attribute:"+setBits);
            	   for(int i=0;i<data.length;i++){
            		   int sample=(int) (data[i]*setBits);
            		   System.out.println("Simple Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample + " Percentage :"+data[i]);            		  
            		   testSimpleAggregationWithoutReplacement("Day","ArrDelay",sample, NTRIALS);     
            		   testSimpleAggregationWithoutReplacementMultipleRounds("Day","ArrDelay",sample, NTRIALS);     
            	   } 
            	   
            	   for(int i=0;i<data.length-3;i++){
            		   int sample=(int) (data[i]*db.rowCount);
            		   System.out.println("Simple Aggregation: Sampling "+(data.length+i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
            		   testSimpleAggregationWithoutReplacement("Day","ArrDelay",sample, NTRIALS);     
            		   testSimpleAggregationWithoutReplacementMultipleRounds("Day","ArrDelay",sample, NTRIALS);     
            	   } 
                       
            	   
            	   for(int i=0;i<data.length;i++){
            		   int sample=(int) (data[i]*setBits);
            		   System.out.println("OR Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
            		   testORAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            		   testORAggregationWithoutReplacementMultipleRounds("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            	   }
            	   
            	   for(int i=0;i<data.length-3;i++){
            		   int sample=(int) (data[i]*db.rowCount);
            		   System.out.println("OR Aggregation: Sampling "+(data.length+i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
            		   testORAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            		   testORAggregationWithoutReplacementMultipleRounds("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            	   }
            	   
            	   
            	   int intersectionBits= (int) getAvgIntersectionSize("Day", "FlightCarrier",NTRIALS);
            	   System.out.println("Average number of intersection for Day and flightcarrier attributes:"+intersectionBits);
            	   for(int i=0;i<data.length;i++){
            		   int sample=(int) (data[i]*intersectionBits);
            		   System.out.println("AND Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
            		   testANDAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            		   testANDAggregationWithoutReplacementMultipleRounds("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            	   }
            	   int sample=(int) (db.rowCount*0.25);
            	   System.out.println("AND Aggregation: Sampling "+(data.length+1)+": with a sample size of "+ sample+" Percentage :"+sample);            		  
            	   testANDAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
        		   testANDAggregationWithoutReplacementMultipleRounds("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
            	   
               }
               else{                                          
                   testSimpleAggregationWithoutReplacement("Day","ArrDelay",sample_size, NTRIALS);           
                   testORAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", sample_size, NTRIALS);
                   testANDAggregationWithoutReplacement("Day", "FlightCarrier", "ArrDelay", intersection_sample_size, NTRIALS);            
               }
         	
         } 
        
        
        

	
       public static void testWithReplacement(){
        	 int setBits= getAvgSetBits("Day",NTRIALS);
         	   int samplet=(int) (0.01*setBits);
       	    System.out.println("Warming up Dry run: Simple Aggregation: Sampling : with a sample size of "+ samplet + " Percentage :"+0.01);            		  
       	   testSimpleAggregation("Day","ArrDelay",samplet, NTRIALS);             		
         	
         	
             int count=0;
        	  if(multiple==1){
           	   double [] data = new double [] {.01,.10,.25,.50,.75,.90,1};
           	   System.out.println("Average number of setbits per Roaring bitmap for Day attribute:"+setBits);
           	   for(int i=0;i<data.length;i++){
           		   int sample=(int) (data[i]*setBits);
           		   System.out.println("Simple Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample + " Percentage :"+data[i]);            		  
           		   testSimpleAggregation("Day","ArrDelay",sample, NTRIALS);      
           	   } 
           	   
           	   for(int i=0;i<data.length-3;i++){
           		   int sample=(int) (data[i]*db.rowCount);
           		   System.out.println("Simple Aggregation: Sampling "+(data.length+i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
           		   testSimpleAggregation("Day","ArrDelay",sample, NTRIALS);      
           	   } 
                      
           	   
           	   for(int i=0;i<data.length;i++){
           		   int sample=(int) (data[i]*setBits);
           		   System.out.println("OR Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
           		   testORAggregation("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
           	   }
           	   
           	   for(int i=0;i<data.length-3;i++){
           		   int sample=(int) (data[i]*db.rowCount);
           		   System.out.println("OR Aggregation: Sampling "+(data.length+i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
           		   testORAggregation("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
           	   }
           	   
           	   
           	   int intersectionBits= (int) getAvgIntersectionSize("Day", "FlightCarrier",NTRIALS);
           	   System.out.println("Average number of intersection for Day and flightcarrier attributes:"+intersectionBits);
           	   for(int i=0;i<data.length;i++){
           		   int sample=(int) (data[i]*intersectionBits);
           		   System.out.println("AND Aggregation: Sampling "+(i+1)+": with a sample size of "+ sample+" Percentage :"+data[i]);            		  
           		   testANDAggregation("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
           	   }
           	   int sample=(int) (db.rowCount*0.25);
           	   System.out.println("AND Aggregation: Sampling "+(data.length+1)+": with a sample size of "+ sample+" Percentage :"+sample);            		  
       		   testANDAggregation("Day", "FlightCarrier", "ArrDelay", sample, NTRIALS);
           	   
              }
              else{                                          
                  testSimpleAggregation("Day","ArrDelay",sample_size, NTRIALS);           
                  testORAggregation("Day", "FlightCarrier", "ArrDelay", sample_size, NTRIALS);
                  testANDAggregation("Day", "FlightCarrier", "ArrDelay", intersection_sample_size, NTRIALS);            
              }
        	
        }
        
        
        private static void loadData(String dataset, String schema, int limit)
				throws IOException, InterruptedException {
		    long start=System.currentTimeMillis();
			if (limit != -1)
			 db = new Database("test", schema, dataset,limit);
			else
			 db = new Database("test", schema, dataset);
			long duration=System.currentTimeMillis()-start;		
			samplingBasedExecutor=new SamplingBasedExecutor(db);
			executor = new Executor(db);
			printDbDetails(db,duration);
			
		}
		
		
		private static void printDbDetails(Database db, long time){
			
			long row_count=db.rowCount;
			long numberofColumns=db.getColumns().size();		
			if(!sizeof)
				return;
		
			long dbSize = SizeOf.deepSizeOf(db);
			System.out.println("########");
            System.out.println("Number of rows in the database: "+row_count);
            System.out.println("Number of columns in the database: "+numberofColumns);
            System.out.println("Size of the database: "+dbSize);  
            System.out.println("Time taken to load the database: "+time);  
            System.out.println("########");			
		}
		
		
		
	 public static int getAvgSetBits(String groupbyAttribute,int numberofruns){			
			Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute);
			double groupbySamplingTime=0.0;
			double sampledAggregationTime=0.0;
			double unsampledAggregationTime=0.0;
			int count=0;
			for(int i=0;i<numberofruns;i++){
				int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
				int c=0;
				String value="";
				for(String key:indexedbitmaps.keySet()){
					if (c < randomInt){
					 	c++; 
						continue;						
					}
				    value=key;
				    break;
				}
			
			  RoaringBitmap bitmap = samplingBasedExecutor.getSimpleGroup(groupbyAttribute, value);
			  count=count+bitmap.getCardinality();
        	}
			
			return count/numberofruns;
		}		
		
		
		
	public static void testSimpleAggregation(String groupbyAttribute,String measureAtttribute,int numberofSamples,int numberofruns){			
			Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute);
			double groupbySamplingTime=0.0;
			double sampledAggregationTime=0.0;
			double unsampledAggregationTime=0.0;
			int sampleduniqueContainers=0;
			int sampledtotalContainers=0;
			int totalConatiners=0;
			for(int i=0;i<numberofruns;i++){
				int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
				int c=0;
				String value="";
				for(String key:indexedbitmaps.keySet()){
					if (c < randomInt){
					 	c++; 
						continue;						
					}
				    value=key;
				    break;
				}
			
			  long start=System.nanoTime();
			  SampledResult sampledResult = samplingBasedExecutor.getSampledSimpleGroup(groupbyAttribute,value, numberofSamples);
	//		  System.out.println(sampledResult.sampledContainers.size());
			 
    		  long second=System.nanoTime();
			  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
			  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
			  long third=System.nanoTime();
			  RoaringBitmap roaringBitmap = samplingBasedExecutor.getSimpleGroup(groupbyAttribute,value);		
			  samplingBasedExecutor.getSimpleAggregate(roaringBitmap, measureAtttribute,"avg");
			  long fourth=System.nanoTime();		
			  sampleduniqueContainers=sampleduniqueContainers+sampledResult.sampledContainers.size();
			  sampledtotalContainers=sampledtotalContainers+sampledResult.totalContainers;
			  totalConatiners=totalConatiners+roaringBitmap.highLowContainer.size();
			  groupbySamplingTime=groupbySamplingTime+second-start;
			  sampledAggregationTime=sampledAggregationTime+third-second;
			  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
	}
			
			groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
			sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
			unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
			sampleduniqueContainers=sampleduniqueContainers/numberofruns;
			totalConatiners=totalConatiners/numberofruns;
			sampledtotalContainers=sampledtotalContainers/numberofruns;
			System.out.println("##Testing Simple GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
            System.out.println("Average time for sampling group by  "+groupbySamplingTime);
            System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
            System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
            System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
            System.out.println("Average number of sampled unique Containers:"+sampleduniqueContainers);
            System.out.println("Average number of sampled total Containers:"+sampledtotalContainers);
            System.out.println("Average number of total containers:"+totalConatiners);
            System.out.println("########");		
            
           
		}		
	
	public static void testSimpleAggregationWithoutReplacement(String groupbyAttribute,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		int sampleduniqueContainers=0;
		int sampledtotalContainers=0;
		int totalConatiners=0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
		
		  long start=System.nanoTime();
		  SampledResult sampledResult = samplingBasedExecutor.getSampledSimpleGroupWithoutReplacement(groupbyAttribute,value, numberofSamples);
//		  System.out.println(sampledResult.sampledContainers.size());
		 
		  long second=System.nanoTime();
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  long third=System.nanoTime();
		  RoaringBitmap roaringBitmap = samplingBasedExecutor.getSimpleGroup(groupbyAttribute,value);		
		  samplingBasedExecutor.getSimpleAggregate(roaringBitmap, measureAtttribute,"avg");
		  long fourth=System.nanoTime();		
		  sampleduniqueContainers=sampleduniqueContainers+sampledResult.sampledContainers.size();
		  sampledtotalContainers=sampledtotalContainers+sampledResult.totalContainers;
		  totalConatiners=totalConatiners+roaringBitmap.highLowContainer.size();
		  groupbySamplingTime=groupbySamplingTime+second-start;
		  sampledAggregationTime=sampledAggregationTime+third-second;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		sampleduniqueContainers=sampleduniqueContainers/numberofruns;
		totalConatiners=totalConatiners/numberofruns;
		sampledtotalContainers=sampledtotalContainers/numberofruns;
		System.out.println("##Testing Simple GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for sampling group by  "+groupbySamplingTime);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("Average number of sampled unique Containers:"+sampleduniqueContainers);
        System.out.println("Average number of sampled total Containers:"+sampledtotalContainers);
        System.out.println("Average number of total containers:"+totalConatiners);
        System.out.println("########");		    
       
	}	
	
	public static double getAvgIntersectionSize(String groupbyAttribute1,String groupbyAttribute2,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	    int count=0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		
			  RoaringBitmap bitmap= samplingBasedExecutor.getANDGroup(groupbyAttribute1,value,groupbyAttribute2,value1);
	    	  count=count+bitmap.getCardinality();
    	}
		
		return count/numberofruns;
		  
}
	
	public static void testSimpleAggregationWithoutReplacementMultipleRounds(String groupbyAttribute,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute);
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		int sampleduniqueContainers=0;
		int sampledtotalContainers=0;
		int totalConatiners=0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
		
		  long start=System.nanoTime();
		  SampledResult sampledResult;
		  sampledResult = samplingBasedExecutor.getSampledSimpleGroupWithoutReplacement(groupbyAttribute,value, numberofSamples);
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  sampleduniqueContainers=sampleduniqueContainers+sampledResult.sampledContainers.size();
		  sampledtotalContainers=sampledtotalContainers+sampledResult.totalContainers;
		  for(int p=0;p<4;p++){
		  sampledResult = samplingBasedExecutor.getSampledSimpleGroupWithoutReplacement(groupbyAttribute,value, numberofSamples,sampledResult.getLastIndex());
//		  System.out.println(sampledResult.sampledContainers.size());
		  measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  sampleduniqueContainers=sampleduniqueContainers+sampledResult.sampledContainers.size();
		  sampledtotalContainers=sampledtotalContainers+sampledResult.totalContainers;
		  
		  }
		  long third=System.nanoTime();
		  for(int p=0;p<5;p++){
		  RoaringBitmap roaringBitmap = samplingBasedExecutor.getSimpleGroup(groupbyAttribute,value);		
		  samplingBasedExecutor.getSimpleAggregate(roaringBitmap, measureAtttribute,"avg");
		  totalConatiners=totalConatiners+roaringBitmap.highLowContainer.size();
		  }
		  long fourth=System.nanoTime();	
		  sampledAggregationTime=sampledAggregationTime+third-start;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
	    sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		sampleduniqueContainers=sampleduniqueContainers/numberofruns;
		totalConatiners=totalConatiners/numberofruns;
		sampledtotalContainers=sampledtotalContainers/numberofruns;
		System.out.println("##Testing Simple GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+(5*numberofSamples));
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("Average number of sampled unique Containers:"+sampleduniqueContainers);
        System.out.println("Average number of sampled total Containers:"+sampledtotalContainers);
        System.out.println("Average number of total containers:"+totalConatiners);
        System.out.println("########");		    
       
	}	
	

	
	  
	public static void testANDAggregation(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getANDSampledGroup(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
//		  System.out.println(sampledResult.sampledContainers.size());
		  long second=System.nanoTime();
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  long third=System.nanoTime();
		  samplingBasedExecutor.getANDAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  long fourth=System.nanoTime();		
		  groupbySamplingTime=groupbySamplingTime+second-start;
		  sampledAggregationTime=sampledAggregationTime+third-second;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for sampling group by  "+groupbySamplingTime);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}	
	
	
	
	public static void testANDAggregationWithoutReplacement(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getANDSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
//		  System.out.println(sampledResult.sampledContainers.size());
		  long second=System.nanoTime();
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  long third=System.nanoTime();
		  samplingBasedExecutor.getANDAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  long fourth=System.nanoTime();		
		  groupbySamplingTime=groupbySamplingTime+second-start;
		  sampledAggregationTime=sampledAggregationTime+third-second;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for sampling group by  "+groupbySamplingTime);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}
	
	
	public static void testANDAggregationWithoutReplacementMultipleRounds(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getANDSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
	
		  for(int p=0;p<4;p++){
			  sampledResult = samplingBasedExecutor.getANDSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples,sampledResult.getLastIndex());
//			  System.out.println(sampledResult.sampledContainers.size());
			  measureValues=db.getUnIndexedColumnValues(measureAtttribute);
			  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
				  
		  }		  
		  long third=System.nanoTime();
		  for(int p=0;p<5;p++){
		  samplingBasedExecutor.getANDAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  }
		  long fourth=System.nanoTime();		
		  sampledAggregationTime=sampledAggregationTime+third-start;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing AND GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}
	
	
	public static void testORAggregation(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
	    	
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getORSampledGroup(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
    	  long second=System.nanoTime();
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  long third=System.nanoTime();
		  samplingBasedExecutor.getORAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  long fourth=System.nanoTime();		
		  groupbySamplingTime=groupbySamplingTime+second-start;
		  sampledAggregationTime=sampledAggregationTime+third-second;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
    }
		
		groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for sampling group by  "+groupbySamplingTime);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}
	
	public static void testORAggregationWithoutReplacement(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getORSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
//		  System.out.println(sampledResult.sampledContainers.size());
		  long second=System.nanoTime();
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
		  long third=System.nanoTime();
		  samplingBasedExecutor.getORAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  long fourth=System.nanoTime();		
		  groupbySamplingTime=groupbySamplingTime+second-start;
		  sampledAggregationTime=sampledAggregationTime+third-second;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		groupbySamplingTime=groupbySamplingTime/(numberofruns* 1e+6);
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+numberofSamples);
        System.out.println("Average time for sampling group by  "+groupbySamplingTime);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}
	
	
	public static void testORAggregationWithoutReplacementMultipleRounds(String groupbyAttribute1,String groupbyAttribute2,String measureAtttribute,int numberofSamples,int numberofruns){			
		Map<String, RoaringBitmap> indexedbitmaps = db.getIndexedColumnValues(groupbyAttribute1);
		Map<String, RoaringBitmap> indexedbitmaps1 = db.getIndexedColumnValues(groupbyAttribute2);
	
		double groupbySamplingTime=0.0;
		double sampledAggregationTime=0.0;
		double unsampledAggregationTime=0.0;
		for(int i=0;i<numberofruns;i++){
			int randomInt = randomGenerator.nextInt(indexedbitmaps.size());
			int c=0;
			String value="";
			for(String key:indexedbitmaps.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value=key;
			    break;
			}
			c=0;
			String value1="";
			randomInt = randomGenerator.nextInt(indexedbitmaps1.size());
			for(String key:indexedbitmaps1.keySet()){
				if (c < randomInt){
				 	c++; 
					continue;						
				}
			    value1=key;
			    break;
			}
		  long start=System.nanoTime();
    	  SampledResult sampledResult = samplingBasedExecutor.getORSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples);
		  String[] measureValues=db.getUnIndexedColumnValues(measureAtttribute);
		  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
	
		  for(int p=0;p<4;p++){
			  sampledResult = samplingBasedExecutor.getORSampledGroupWithoutReplacement(groupbyAttribute1,value,groupbyAttribute2,value1,numberofSamples,sampledResult.getLastIndex());
//			  System.out.println(sampledResult.sampledContainers.size());
			  measureValues=db.getUnIndexedColumnValues(measureAtttribute);
			  samplingBasedExecutor.doAggregateOnSampledBits(sampledResult, measureValues,"avg");
				  
		  }		  
		  long third=System.nanoTime();
		  for(int p=0;p<5;p++){
		  samplingBasedExecutor.getORAggregate(groupbyAttribute1,value,groupbyAttribute2,value1,measureAtttribute,"avg",numberofSamples);
		  }
		  long fourth=System.nanoTime();		
		  sampledAggregationTime=sampledAggregationTime+third-start;
		  unsampledAggregationTime=unsampledAggregationTime+fourth-third;
}
		
		sampledAggregationTime=sampledAggregationTime/(numberofruns* 1e+6);
		unsampledAggregationTime=unsampledAggregationTime/(numberofruns* 1e+6);
		System.out.println("##Testing OR GroupBy## Number of trials: "+numberofruns +"  ## Number of samples: "+5*numberofSamples);
        System.out.println("Average time for aggregating sampled group bys: "+sampledAggregationTime);
        System.out.println("Average time for groupby and aggregation samples  "+(groupbySamplingTime+sampledAggregationTime));
        System.out.println("Average time for  groupby and aggregation without sampling: "+unsampledAggregationTime);           
        System.out.println("########");			
	}
	

}