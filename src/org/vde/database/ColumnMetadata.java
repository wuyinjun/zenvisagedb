package org.vde.database;

public class ColumnMetadata {
  public String name;
  public String dataType;
  public boolean isIndexed;
  public float min = 100000;
  public float max;
  public float pAAWidth = (float) 0.05;

}
