package test;

import java.io.IOException;

import org.vde.database.Database;

public class DatabaseLoader {
	public Database db ;
	
	public void loadData() throws IOException, InterruptedException{
		String name="flightDataset";
		String schemafile="src/airline_schema.csv";
		String datafile="src/airline.csv";
		db = new Database(name, schemafile, datafile);
		
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		DatabaseLoader dataloader = new DatabaseLoader();
		dataloader.loadData();

		
	}

}
